percentage = ->
  decimals = if it < 0.01 then 1 else 0
  "#{window.ig.utils.formatNumber it * 100, decimals}&nbsp;%"

container = d3.select ig.containers.base
mapElement = container.append \div
  ..attr \class \map

map = L.map do
  * mapElement.node!
  * minZoom: 7,
    maxZoom: 14,
    zoom: 7,
    center: [49.78, 15.5]
    maxBounds: [[48.3,11.6], [51.3,19.1]]

baseLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_b1/{z}/{x}/{y}.png"
  * zIndex: 1
    opacity: 1
    attribution: 'data <a href="https://www.czso.cz/" target="_blank">ČSÚ</a>, mapová data &copy; přispěvatelé <a target="_blank" href="http://osm.org">OpenStreetMap</a>, obrazový podkres <a target="_blank" href="http://stamen.com">Stamen</a>, <a target="_blank" href="https://samizdat.cz">Samizdat</a>'

labelLayer = L.tileLayer do
  * "https://samizdat.cz/tiles/ton_l1/{z}/{x}/{y}.png"
  * zIndex: 3
    opacity: 0.75
kandidati =
  * id: "bobo"
    name: "Jana Bobošíková"
    winnersColor: "rgb(165,15,21)"
    colors: ['rgb(255,245,240)','rgb(254,224,210)','rgb(252,187,161)','rgb(252,146,114)','rgb(251,106,74)','rgb(239,59,44)','rgb(203,24,29)','rgb(165,15,21)','rgb(103,0,13)']
    values: [0, 0, 0.015625, 0.03125, 0.046875, 0.0625, 0.078125, 0.09375, 0.3333333333333333]
  * id: "dienst"
    name: "Jiří Dienstbier"
    winnersColor: \#f29400
    colors: ['rgb(255,255,204)','rgb(255,237,160)','rgb(254,217,118)','rgb(254,178,76)','rgb(253,141,60)','rgb(252,78,42)','rgb(227,26,28)','rgb(189,0,38)','rgb(128,0,38)']
    values: [0, 0.037037037037037035, 0.08389450056116723, 0.13075196408529743, 0.1776094276094276, 0.2244668911335578, 0.271324354657688, 0.3181818181818182, 0.6511627906976745]
  * id: "fisch"
    name: "Jan Fischer"
    winnersColor: \#1C76F0
    colors: ['rgb(255,247,251)','rgb(236,226,240)','rgb(208,209,230)','rgb(166,189,219)','rgb(103,169,207)','rgb(54,144,192)','rgb(2,129,138)','rgb(1,108,89)','rgb(1,70,54)']
    values: [0, 0.043478260869565216, 0.0913499942941915, 0.13922172771881774, 0.18709346114344402, 0.2349651945680703, 0.28283692799269655, 0.3307086614173228, 0.6666666666666666]
  * id: "franz"
    name: "Vladimír Franz"
    winnersColor: \#5434A3
    colors: ['rgb(255,247,251)','rgb(236,231,242)','rgb(208,209,230)','rgb(166,189,219)','rgb(116,169,207)','rgb(54,144,192)','rgb(5,112,176)','rgb(4,90,141)','rgb(2,56,88)']
    values: [0, 0, 0.028169014084507043, 0.056338028169014086, 0.08450704225352113, 0.11267605633802817, 0.14084507042253522, 0.16901408450704225, 0.5625]
  * id: "roit"
    name: "Zuzana Roithová"
    winnersColor: \#FEE300
    colors: ['rgb(255,255,229)','rgb(255,247,188)','rgb(254,227,145)','rgb(254,196,79)','rgb(254,153,41)','rgb(236,112,20)','rgb(204,76,2)','rgb(153,52,4)','rgb(102,37,6)']
    values: [0, 0, 0.03005464480874317, 0.06010928961748634, 0.09016393442622951, 0.12021857923497269, 0.15027322404371585, 0.18032786885245902, 0.46153846153846156]
  * id: "schwarz"
    name: "Karel Schwarzenberg"
    winnersColor: \#B560F3
    colors: ['rgb(247,252,253)','rgb(224,236,244)','rgb(191,211,230)','rgb(158,188,218)','rgb(140,150,198)','rgb(140,107,177)','rgb(136,65,157)','rgb(129,15,124)','rgb(77,0,75)']
    values: [0, 0.01680672268907563, 0.10241139934234562, 0.18801607599561562, 0.27362075264888563, 0.3592254293021556, 0.44483010595542566, 0.5304347826086957, 0.7170111287758346]
  * id: "sobot"
    name: "Přemysl Sobotka"
    winnersColor: \#504E4F
    colors: ['rgb(247,251,255)','rgb(222,235,247)','rgb(198,219,239)','rgb(158,202,225)','rgb(107,174,214)','rgb(66,146,198)','rgb(33,113,181)','rgb(8,81,156)','rgb(8,48,107)']
    values: [0, 0, 0.013725490196078431, 0.027450980392156862, 0.041176470588235294, 0.054901960784313725, 0.06862745098039215, 0.08235294117647059, 0.26666666666666666]
  * id: "tana"
    name: "Táňa Fischerová"
    winnersColor: \#0FB103
    colors: ['rgb(247,244,249)','rgb(231,225,239)','rgb(212,185,218)','rgb(201,148,199)','rgb(223,101,176)','rgb(231,41,138)','rgb(206,18,86)','rgb(152,0,67)','rgb(103,0,31)']
    values: [0, 0, 0.014814814814814815, 0.02962962962962963, 0.044444444444444446, 0.05925925925925926, 0.07407407407407408, 0.08888888888888889, 0.35]
  * id: "zeman"
    name: "Miloš Zeman"
    winnersColor: \#FB9A99
    colors: ['rgb(255,255,229)','rgb(255,247,188)','rgb(254,227,145)','rgb(254,196,79)','rgb(254,153,41)','rgb(236,112,20)','rgb(204,76,2)','rgb(153,52,4)','rgb(102,37,6)']
    values: [0, 0.08695652173913043, 0.1537645811240721, 0.2205726405090138, 0.2873806998939554, 0.35418875927889715, 0.42099681866383876, 0.4878048780487805, 0.7457627118644068]

grid = new L.UtfGrid "../data/meta/{z}/{x}/{y}.json", useJsonP: no
  ..on \mouseover ({data}:e) ->
    [kod, bobo, dienst, fisch, franz, roit, schwarz, sobot, tana, zeman, pocet] = data
    infobar.displayData {kod, bobo, dienst, fisch, franz, roit, schwarz, sobot, tana, zeman, pocet}
  # ..on \mouseout ->
  #   infobar.reInit!

map
  ..addLayer baseLayer
  ..addLayer labelLayer
  ..addLayer grid
dataLayers = {}
dataLayers["winners"] = L.tileLayer do
    * "../data/winners-prez/{z}/{x}/{y}.png"
    * attribution: '<a href="http://creativecommons.org/licenses/by-nc-sa/3.0/cz/" target = "_blank">CC BY-NC-SA 3.0 CZ</a> <a target="_blank" href="http://rozhlas.cz">Rozhlas.cz</a>, data <a target="_blank" href="http://www.volby.cz">ČSÚ</a>'
      opacity: 0.9
      zIndex: 2
dataLayers["winners-amount"] = L.tileLayer do
    * "../data/winners-amount-prez/{z}/{x}/{y}.png"
    * attribution: '<a href="http://creativecommons.org/licenses/by-nc-sa/3.0/cz/" target = "_blank">CC BY-NC-SA 3.0 CZ</a> <a target="_blank" href="http://rozhlas.cz">Rozhlas.cz</a>, data <a target="_blank" href="http://www.volby.cz">ČSÚ</a>'
      zIndex: 2
kandidati_assoc = {}
for kandidat in kandidati
  kandidati_assoc[kandidat.id] = kandidat
  dataLayers[kandidat.id] = L.tileLayer do
    * "../data/h-#{kandidat.id}-prez/{z}/{x}/{y}.png"
    * attribution: '<a href="http://creativecommons.org/licenses/by-nc-sa/3.0/cz/" target = "_blank">CC BY-NC-SA 3.0 CZ</a> <a target="_blank" href="http://rozhlas.cz">Rozhlas.cz</a>, data <a target="_blank" href="http://www.volby.cz">ČSÚ</a>'
      opacity: 0.9
      zIndex: 2
currentLayer = dataLayers['winners']
  ..addTo map
infobar = new ig.Infobar container, kandidati

legend = container.append \div .attr \class "legend hidden"
legendValues = legend.selectAll \div .data kandidati.0.colors .enter!append \div
  ..attr \class \field
  ..html (d, i) -> i
  ..style \background-color -> it

container.append \select
  ..selectAll \option .data ([{name: "Vítězové voleb", id: \winners}, {name: "Vítězové (sytost podle náskoku)", id: \winners-amount}] ++ kandidati) .enter!append \option
    ..html (.name)
    ..attr \value (.id)
  ..on \change ->
    map.removeLayer currentLayer
    currentLayer := dataLayers[@value]
      ..addTo map
    kandidat = kandidati_assoc[@value]
    legend.classed \hidden !kandidat
    if kandidat
      legendValues.style \background-color (d, i) -> kandidat.colors[i]
      legendValues.html (d, i) ->
        p = percentage kandidat.values[i]
        if i != 8
          p = p.split "&nbsp;" .0
        p



(err, text) <~ d3.text "/tools/suggester/0.0.1/okresy_obce.tsv"
[okresy, obce] = text.split "\n\n"
okresy_assoc = {}
okresy.split "\n"
  .map (.split "\t")
  .forEach ([kod, nazev]) -> okresy_assoc[kod] = {kod, nazev}
obce_assoc = {}
obce = for line in obce.split "\n"
  [lon, lat, id, okres_kod, nazev] = line.split "\t"
  okres = okresy_assoc[okres_kod]
  lat = parseFloat lat
  lon = parseFloat lon
  id = parseInt id, 10
  nazevSearchable = nazev.toLowerCase!
  obce_assoc[id] = {lat, lon, id, okres, nazev, nazevSearchable}

infobar.obce = obce_assoc

suggesterContainer = container.append \div
  ..attr \class \suggesterContainer
  ..append \span .html "Najít obec"
selectedOutline = null

setOutline = (iczuj) ->
  if selectedOutline
    map.removeLayer selectedOutline
  (err, data) <~ d3.json "/tools/suggester/0.0.1/geojsons/#{iczuj}.geo.json"
  return unless data
  style =
    fill: no
    opacity: 1
    color: '#000'
  selectedOutline := L.geoJson data, style
    ..addTo map

new ig.Suggester suggesterContainer, obce
  ..on 'selected' (obec) ->
      map.setView [obec.lat, obec.lon], 12
      setOutline obec.id

